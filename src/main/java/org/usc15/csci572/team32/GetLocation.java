package org.usc15.csci572.team32;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import com.bericotech.clavin.GeoParser;
import com.bericotech.clavin.GeoParserFactory;
import com.bericotech.clavin.resolver.ResolvedLocation;
import com.bericotech.clavin.util.TextUtils;

public class GetLocation {

	public static void main(String[] args) throws Exception {
		if(args.length != 1){
			System.out.println("Invalid number of arguments");
			System.exit(0);
		}
		GeoParser parser = GeoParserFactory.getDefault("src/main/resources/IndexDirectory");
		File inputFile = new File(args[0]);
		String inputString = TextUtils.fileToString(inputFile);
		List<ResolvedLocation> resolvedLocations = parser.parse(inputString);
		HashMap<String, LocationValueObject> locOccurenceCount = new HashMap<String, LocationValueObject>();
		String locOfDoc = "Unknown";
		double latitude = 0;
		double longitude = 0;
		String countryName = "UNKNOWN";
		locOccurenceCount.put(locOfDoc, new LocationValueObject(countryName, 0,
				0));
		int maxCount = 0;
		for (ResolvedLocation resolvedLocation : resolvedLocations) {
			if (locOccurenceCount.containsKey(resolvedLocation.getGeoname()
					.getName())) {
				int count = 1 + locOccurenceCount.get(
						resolvedLocation.getGeoname().getName()).getCount();
				locOccurenceCount.get(resolvedLocation.getGeoname().getName())
						.setCount(count);
			} else {
				locOccurenceCount.put(resolvedLocation.getGeoname().getName(),
						new LocationValueObject(resolvedLocation.getGeoname()
								.getPrimaryCountryName(), resolvedLocation
								.getGeoname().getLatitude(), resolvedLocation
								.getGeoname().getLongitude()));
			}
			/*
			 * System.out.println(resolvedLocation.getMatchedName());
			 * System.out.println(resolvedLocation.getGeoname().getName());
			 * System.out.println(resolvedLocation.getGeoname().getLatitude());
			 * System.out.println(resolvedLocation.getGeoname().getLongitude());
			 * System
			 * .out.println(resolvedLocation.getGeoname().getAdmin1Code());
			 * System
			 * .out.println(resolvedLocation.getGeoname().getPrimaryCountryName
			 * ());
			 */
		}
		// System.out.println("--------------------------------------------------------------");

		for (String location : locOccurenceCount.keySet()) {
			int count = locOccurenceCount.get(location).getCount();
			//System.out.println(location + " => " + count);
			if (count > maxCount) {
				locOfDoc = location;
				countryName = locOccurenceCount.get(location).getCountryName();
				latitude = locOccurenceCount.get(location).getLatitude();
				longitude = locOccurenceCount.get(location).getLongitude();
				maxCount = count;
			}
		}
		// System.out.println("--------------------------------------------------------------");
		System.out.println("Location:" + locOfDoc);
		if (!locOfDoc.equals("Unknown")) {
			System.out.println("Country:" + countryName);
			System.out.println("Latitude:" + latitude);
			System.out.println("Longitude:" + longitude);
		}
	}
}
