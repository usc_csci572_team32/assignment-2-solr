package org.usc15.csci572.team32;

public class LocationValueObject {
	private int count;
	private String countryName;
	private double latitude;
    private double longitude;
	public LocationValueObject(String primaryCountryName, double latitude, double longitude) {
		this.countryName = primaryCountryName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.count = 1;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
}
