package org.usc15.csci572.team32;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;

import com.bericotech.clavin.GeoParser;
import com.bericotech.clavin.GeoParserFactory;
import com.bericotech.clavin.gazetteer.GeoName;
import com.bericotech.clavin.resolver.ResolvedLocation;

public class SolrIndexer {
	public static final String RES_PATH = "src/main/resources/htmls/";
	public static final String SOLR_SERVER = "http://localhost:8983/solr";

	public static void main(String[] args) throws Exception {
		HttpSolrServer server = new HttpSolrServer(SOLR_SERVER);
		server.deleteByQuery( "*:*" );
		server.commit();
		GeoParser parser = GeoParserFactory.getDefault("./IndexDirectory");
		int i = 0;
		final File folder = new File(RES_PATH);
		Parser _autoParser = new AutoDetectParser();
		for (final File fileEntry : folder.listFiles()) {
			File file = new File(RES_PATH+fileEntry.getName());
			System.out.println(RES_PATH+file.getName());
			ContentHandler textHandler = new BodyContentHandler(-1);
			Metadata metadata = new Metadata();
			ParseContext context = new ParseContext();
			InputStream input = new FileInputStream(file);
			try {
				_autoParser.parse(input, textHandler, metadata, context);
			} catch (Exception e) {
				System.out.println(String.format("File %s failed", file.getCanonicalPath()));
				e.printStackTrace();
				continue;
			}
			//dumpMetadata(file.getCanonicalPath(), metadata);
			SolrInputDocument doc = new SolrInputDocument();
			doc.addField("id", file.getName());
			String title = metadata.get("title");
			if (title != null) {
				doc.addField("title", title);
			}else{
				doc.addField("title", file.getName());
			}
			doc.addField("content", textHandler.toString());
			List<ResolvedLocation> resolvedLocations = parser.parse(textHandler.toString());
			if(resolvedLocations.size()==0)
				System.out.println("nothing here");
			else{
				for (ResolvedLocation resolvedLocation : resolvedLocations){
					System.out.println(resolvedLocation.toString());
		            /*GeoName gn = resolvedLocation.getGeoname();
		            System.out.println("Name: " + gn.getPreferredName());
		            System.out.println("Lat: " + gn.getLatitude());
		            System.out.println("Long: " + gn.getLongitude());*/
				}
			}
			server.add(doc);
			if (i == 100) {
				server.commit();
				i = 0;
			}
			i++;
		}
		server.commit();
		
		/*for (int i = 0; i < 1000; ++i) {
			SolrInputDocument doc = new SolrInputDocument();
			doc.addField("cat", "book");
			doc.addField("id", "book-" + i);
			doc.addField("name", "The Legend of the Hobbit part " + i);
			server.add(doc);
			if (i % 100 == 0)
				server.commit(); // periodically flush
		}
		server.commit();*/
	}
}
