package org.usc15.csci572.team32;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

public class SolrSearcher {

	private static final SolrServer SOLR = new HttpSolrServer(SolrIndexer.SOLR_SERVER);
	private static SolrQuery query = new SolrQuery();
	private static String queryString = "*";
	private static int pageNumber = 0;
	private static int noOfRows = 15;
	private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

	public static void main(String[] args) throws SolrServerException, IOException{

		String title = null;
		QueryResponse response = null;
		SolrDocumentList results = null;

		while (true) {
			int choice = chooseOption(queryString);

			switch (choice) {
			case 1:
				queryString = getQueryString();
				pageNumber = 0;
				title = null;
				break;
			case 2:
				updateResultsPerPage();
				break;
			case 3:
				pageNumber--;
				if (pageNumber < 0)
					pageNumber = 0;
				break;
			case 4:
				pageNumber++;
				break;
			case 5:
				title = getTitle();
				break;
			case 6:
				reader.close();
				System.exit(0);
			default:
				System.out.println("Invalid choice !");
				continue;
			}

			if (!queryString.equals("*")) {
				if (title != null){
					SolrQuery query2 = new SolrQuery();
					query2.setQuery(queryString);
					query2.addFilterQuery("id:" + title);
					query2.setFields("id", "title", "content");
					response = SOLR.query(query2);
					results = response.getResults();
					for (SolrDocument doc : results){
						System.out.println(doc.getFieldValue("content"));
						Desktop.getDesktop().open(new File(SolrIndexer.RES_PATH+doc.getFieldValue("id")));
					}
				}
				query.setQuery(queryString);
				query.setFields("title", "id");
				query.setStart(pageNumber * noOfRows);
				query.setRows(noOfRows);
				response = SOLR.query(query);
				results = response.getResults();
				Map<String, Object> debug_map = response.getDebugMap();
				Map<String, String> explain_map = response.getExplainMap();
				System.out.println(debug_map);
				System.out.println(explain_map);
				for (SolrDocument doc : results){
					System.out.println(doc.getFieldValue("id") + "=>" + doc.getFieldValue("title"));
				}
			}
		}
	}

	private static String getTitle() throws IOException {
		System.out.println("Enter the page title:");
		return reader.readLine();
	}

	private static void updateResultsPerPage() throws IOException {
		System.out.println("Enter number of results per page:");
		int n = noOfRows;
		try{
			n = Integer.parseInt(reader.readLine());
		}catch(NumberFormatException e){
			System.out.println("Invalid entry !");
			return;
		}
		noOfRows = n;
		if (noOfRows < 1)
			noOfRows = 15;
	}

	private static String getQueryString() throws IOException {
		String queryString = "*";
		System.out.println("Enter a query string:");
		queryString = reader.readLine();
		return queryString;
	}

	private static int chooseOption(String queryString) throws IOException {
		System.out.println("Enter a choice:");
		System.out.println("1. Enter query string");
		if (!queryString.equals("*")) {
			System.out.println("2. Enter number of results per page");
			System.out.println("3. Prev page of results");
			System.out.println("4. Next page of results");
			System.out.println("5. Select result id");
		}
		System.out.println("6. Quit");
		int ch = -1;
		try{
			ch = Integer.parseInt(reader.readLine());
		}catch(NumberFormatException e){
		}
		return ch;
	}

}
