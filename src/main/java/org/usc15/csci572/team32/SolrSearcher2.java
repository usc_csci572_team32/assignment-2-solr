package org.usc15.csci572.team32;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class SolrSearcher2 {

	public static void main(String[] args) {
		try {
			URL urldemo = new URL("http://localhost:8983/solr/select?debugQuery=on&fl=title&q=mul(tf(content,Lindsay),idf(content,Lindsay))");
			URLConnection yc = urldemo.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					yc.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null)
				System.out.println(inputLine);
			in.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
