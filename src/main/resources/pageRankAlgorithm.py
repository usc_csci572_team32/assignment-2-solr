import argparse
import os
import math


def read_JSON_file(files):
    nodes = []
    #code for read JSON file here
    for file in files:
        locations = ['Alaska', 'United States', '112, 86', 'Alaska']
        temporal = [2015, 2014]
        topics = ['ice cap', 'polar bear']
        node = {"filename": file, "locations": set(locations), "temporal": set(temporal), "topics": set(topics)}
        nodes.append(node)
    return nodes


def get_meta_data(nodes):
    metadata = {}
    for i in range(len(nodes)):
        page_rank = 0
        for j in range(len(nodes)):
            if i != j:
                page_rank += math.sqrt(
                    (len(nodes[i]['locations'] & nodes[j]['locations'])) ** 2 +
                    (len(nodes[i]['temporal'] & nodes[j]['temporal'])) ** 2 +
                    (len(nodes[i]['topics'] & nodes[j]['topics'])) ** 2
                )
        metadata[nodes[i]['filename']] = {"page_rank": page_rank, "locations": nodes[i]['locations'],
                                          "temporal": nodes[i]['temporal'], "topics": nodes[i]['topics']}
    return metadata


def page_ranker(json_file):
    nodes = read_JSON_file(json_file)
    for i in range(len(nodes)):
        print(nodes[i])
    meta_data = get_meta_data(nodes)
    for filename in meta_data:
        print(filename)
        for key in meta_data[filename]:
            print(key, ": ", meta_data[filename][key])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("JSON_file", help="JSON_file")
    arguments = parser.parse_args()

    json_file = arguments.JSON_file

    page_ranker(json_file)