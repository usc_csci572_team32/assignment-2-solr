#!/bin/bash

#Usage:
#upload.sh <dumpFolder>
#     dumpFolder: contains all the files to be indexed

curl "http://localhost:8983/solr/update?stream.body=<delete><query>*:*</query></delete>"
curl "http://localhost:8983/solr/update?stream.body=<commit/>"

count=0
for file in $1/*; do
	curl "http://localhost:8983/solr/update/extract?literal.id=csci572doc$count&commit=true" -F "myfile=@$file"
	((count++))
done
